//
//  ViewController.swift
//  PayPalSDK

//

import UIKit

class ViewController: UIViewController, PayPalPaymentDelegate {
    var config = PayPalConfiguration()
                            
    @IBAction func buyClicked(sender : AnyObject) {
        // A constant which is used to store amount of money which have to pay...
        let amount = NSDecimalNumber(string: "10.00")
        println("amount \(amount)")
        
        var payment = PayPalPayment()
        payment.amount = amount
        payment.currencyCode = "EUR"
        payment.shortDescription = "Swift payment"
        
        //code block to check wether payment is possible in term of money...
        if (!payment.processable) {
            println("You messed up!")
        } else {
            //code to open Paypal app controller which provides methods for online payment process...
            println("This works")
            var paymentViewController = PayPalPaymentViewController(payment: payment, configuration: config, delegate: self)
            self.presentViewController(paymentViewController, animated: false, completion: nil)
        }
    }
    
    //code block used to return app after payment successfully...
    func payPalPaymentViewController(paymentViewController: PayPalPaymentViewController!, didCompletePayment completedPayment: PayPalPayment!) {
        
        self.dismissViewControllerAnimated(true, completion: nil)//code to dismiss payment controller...
        
        // shows alert after successful payment...
        println("Payment successful...")
        let title = "Done!!"
        let message = " Payment successful..."
        let okText = "Ok"
        let alert = UIAlertController(title: title, message: message, preferredStyle: UIAlertControllerStyle.Alert)
        let okButton = UIAlertAction(title: okText, style: UIAlertActionStyle.Cancel, handler: nil)
        alert.addAction(okButton)
        presentViewController(alert, animated: true, completion: nil)
    }
    //code for cencel button...
    func payPalPaymentDidCancel(paymentViewController: PayPalPaymentViewController!) {
        self.dismissViewControllerAnimated(true, completion: nil)
    }
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(true);
        PayPalMobile.preconnectWithEnvironment(PayPalEnvironmentNoNetwork)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
   }

